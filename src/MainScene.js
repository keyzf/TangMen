var MainSceneLayer = cc.Layer.extend({
	sprite : null,
	spriteBird : null,
	ctor : function() {
		this._super();
		var size = cc.winSize;
		this.sprite = new cc.Sprite(res.HelloWorld_png);
		this.sprite.attr({
			x : size.width / 2,
			y : size.height / 2,
			scale : 0.5,
			rotation : 180
		});
		this.addChild(this.sprite,0);
		this.sprite.runAction(
				cc.Sequence(
						cc.RotateTo(2, 55),
						cc.scaleTo(2, 1, 0.5),
						cc.scaleTo(1, 0.5, 0.5),
						cc.scaleTo(1,0.5,1),
						cc.scaleTo(1,0.5,0.5)
				)
		);
		this.sprite.runAction(
				cc.Spawn(
						cc.RotateBy(2,180)
				)
		);
		this.spriteBird = new cc.Sprite(res.bird_png);
		this.spriteBird.attr({
			x : size.width / 2,
			y : size.height /2,
			scale : 1.0,
			rotation : 0
			
		});
		this.addChild(this.spriteBird);
		this.spriteBird.runAction(
				cc.Spawn(
						cc.MoveTo(2,cc.p(size.width - 50, size.height - 40))
				)
		);
		var helo = new cc.LabelTTF("Hello World","Arial",38);
		helo.x = size.width / 2;
		helo.y = 0;
		this.addChild(helo, 5);
		helo.runAction(
				cc.spawn(
						cc.moveBy(2.5, cc.p(0, size.height - 40)),
						cc.tintTo(2.5,255,125,0)
				)
		);
		return true;
		
	}
	
});
var MainScene = cc.Scene.extend({
	onEnter : function () {
		this._super();
		var layer = new MainSceneLayer();
		this.addChild(layer);
	}
	
});